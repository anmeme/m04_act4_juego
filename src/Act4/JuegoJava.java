package Act4;

import java.util.Random;
import java.util.Scanner;

public class JuegoJava {
	public static void main(String[] args) {
		boolean end = false;
		boolean ganador = false;
		boolean perdedor = false;
		int numeroGuanyador = 0;
		int aposta = 0;

		Scanner sc = new Scanner(System.in);
		System.out.println("Escribe tu nombre:");
		String nom = sc.nextLine();
		System.out.println("Bienvenido " + nom);

		while (end == false) {
			System.out.println("--------------------");
			System.out.println("Selecciona una opci�n:");
			System.out.println("1. Instrucciones de como se juega.");
			System.out.println("2. Jugar");
			System.out.println("3. Resultados de la ultima partida.");
			System.out.println("4. Salir.");
			int opcio = sc.nextInt();

			switch (opcio) {
			case 1:
				System.out.println(
						"- El programa genera un numero aleatorio entre 1 y 10. El objetivo es adivinarlo. \n- Introduce un numero para empezar y si lo aciertas, ganas. Si no, se indica que has perdido y el numero que habia que acertar.\n");
				break;

			case 2:
				Random rnd = new Random();
				numeroGuanyador = rnd.nextInt(1, 10);

				System.out.println("Elige un numero entre 1 y 10. (0 para rendirte)");
				do {
					
					aposta = sc.nextInt();
					if (aposta == numeroGuanyador) {
						System.out.println("Has ganado! Felicidades " + nom + "!\n");
						ganador = true;
						perdedor = false;
						break;
					} else {
						System.out.println("No es correcto, vuelve a intentarlo:");
						perdedor = true;
						ganador = false;
					}
				} while (aposta != numeroGuanyador || aposta != 0);
				break;

			case 3:
				if (ganador == true) {
					System.out.println("En la ultima partida GANASTE. El numero acertado era el " + numeroGuanyador + "\n");
				} else if (perdedor == true) {
					System.out.println("En la ultima partida PERDISTE. El numero correcto era el " + numeroGuanyador + "\n");
				} else {
					System.out.println("No has jugado aun...");
				}
				break;

			case 4:
				System.out.println("Adeu!");
				end = true;
				break;

			default:
				System.out.println("La opcion elegida es incorrecta!");
			}
		}
		sc.close();
	}
}